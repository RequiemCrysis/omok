//main.cpp
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include "omok.h"

using namespace std;

struct can_use
{
	int x, y;
};

int put_max(int num, int max){
	if(num > max) max = num;
	return max;
}

int main(void) 
{
	vector< vector<char> > board(19,vector<char>(19,'.'));
	vector <shape> save;
	vector <can_use> use;

	int score[19][19] = {0, };

	info inf;
	info inf2;

	int dirlist[12][2] = {{0, 1}, {0, 2}, {0, 3}, {1, 2}, {1, 3}, {2, 3}, {1, 0}, {2, 0}, {3, 0}, {2, 1}, {3, 1}, {3, 2}};

	int max = 0;
	int max2 = 0;
	int cx,cy;
	int my_x=10, my_y=10;
	int posx, posy;
	char my='o', opp='x';		// default : 내가 백, 상대 흑
	int flag = 0;


	cin >> cx >> cy;

	if(cx==0 && cy ==0){
		flag = 1;
		my = 'x';					// 내가 흑
		opp = 'o';					// 상대가 백
		cout << my_x << my_y;
		board[my_y][my_x] = my;  
	//	cin >> cx >> cy;
	//	cx--, cy--;  
	}

	//	board[cy][cx] = opp_type;
	while(true){
		if(flag){
			cin >> cx >> cy;
		}
		flag = 1;

		board[--cx][--cy] == opp;


		max2 = 0;

		//자신의 경우부터
		for(int i = 0; i < 19; i++){
			for(int j = 0; j < 19; j++){
				max = 0;
				for(int dir = 0; dir < 4; dir++){
					inf = getinfo(dir, j, i, my, opp, board);

					//한줄
					if(inf.length == 5) score[j][i] = put_max(100, score[j][i]);	//오목
					if(inf.length == 4 && inf.isblocked == 0 && inf.isblank == 0) score[j][i] = put_max(69, score[j][i]);
					if(inf.length == 4 && inf.isblocked == 0 && inf.isblank == 1) score[j][i] = put_max(33, score[j][i]);
					if(inf.length == 4 && inf.isblocked == 1 && inf.isblank == 1) score[j][i] = put_max(32, score[j][i]); 
					if(inf.length == 4 && inf.isblocked == 2 && inf.isblank == 1) score[j][i] = put_max(31, score[j][i]);
					if(inf.length == 4 && inf.isblocked == 1 && inf.isblank == 0) score[j][i] = put_max(9, score[j][i]);
					if(inf.length == 3 && inf.isblocked == 0 && inf.isblank == 0) score[j][i] = put_max(10, score[j][i]);
					if(inf.length == 3 && inf.isblocked == 0 && inf.isblank == 1) score[j][i] = put_max(7, score[j][i]);
					if(inf.length == 3 && inf.isblocked == 0 && inf.isblank == 1) score[j][i] = put_max(6, score[j][i]);
					if(inf.length == 2 && inf.isblocked == 0 && inf.isblank == 0) score[j][i] = put_max(2, score[j][i]);
					if(inf.length == 2 && inf.isblocked == 1 && inf.isblank == 0) score[j][i] = put_max(1, score[j][i]);
				}

				for(int k = 0; k < 12; k++){
					inf = getinfo(dirlist[k][0], j, i, my, opp, board);
					inf2 = getinfo(dirlist[k][1], j, i, my, opp, board);

					if(inf.length == 4 && inf2.length == 4 && inf.isblank < 2 && inf2.isblank < 2) score[j][i] = put_max(99, score[j][i]);
					if(inf.length == 4 && inf2.length == 3 && inf2.isblocked == 0 && inf.isblank < 2 && inf2.isblank < 2) score[j][i] = put_max(98, score[j][i]);
					if(inf.length == 3 && inf.isblocked == 0 && inf2.length == 3 && inf2.isblocked == 0 && inf.isblank < 2 && inf2.isblank < 2) score[j][i] = put_max(30, score[j][i]);
					if(inf.length == 3 && inf.isblocked == 0 && inf2.length == 2 && inf2.isblocked == 0 && inf.isblank < 2) score[j][i] = put_max(21, score[j][i]);
					if(inf.length == 3 && inf.isblocked == 1 && inf2.length == 3 && inf2.isblocked == 0 && inf.isblank < 2 && inf2.isblank < 2) score[j][i] = put_max(8, score[j][i]);
					if(inf.length == 2 && inf.isblocked == 0 && inf2.length == 2 && inf2.isblocked == 0) score[j][i] = put_max(5, score[j][i]);
				}

			}
		}

		for(int i = 0; i < 19; i++){
			for(int j = 0; j < 19; j++){
				max2 = put_max(score[j][i], max2);
				if(max2 == score[j][i]){
					posx = j;
					posy = i;
				}
			}
		}

		//이번에는 상대
		for(int i = 0; i < 19; i++){
			for(int j = 0; j < 19; j++){
				score[j][i] = 0;
				for(int dir = 0; dir < 4; dir++){
					inf = getinfo(dir, j, i, opp, my, board);

					//한줄
					if(inf.length == 5) score[j][i] = put_max(100, score[j][i]);	//오목
					if(inf.length == 4 && inf.isblocked == 0 && inf.isblank == 0) score[j][i] = put_max(69, score[j][i]);
					if(inf.length == 4 && inf.isblocked == 0 && inf.isblank == 1) score[j][i] = put_max(33, score[j][i]);
					if(inf.length == 4 && inf.isblocked == 1 && inf.isblank == 1) score[j][i] = put_max(32, score[j][i]); 
					if(inf.length == 4 && inf.isblocked == 2 && inf.isblank == 1) score[j][i] = put_max(31, score[j][i]);
					if(inf.length == 4 && inf.isblocked == 1 && inf.isblank == 0) score[j][i] = put_max(9, score[j][i]);
					if(inf.length == 3 && inf.isblocked == 0 && inf.isblank == 0) score[j][i] = put_max(10, score[j][i]);
					if(inf.length == 3 && inf.isblocked == 0 && inf.isblank == 1) score[j][i] = put_max(7, score[j][i]);
					if(inf.length == 3 && inf.isblocked == 0 && inf.isblank == 1) score[j][i] = put_max(6, score[j][i]);
					if(inf.length == 2 && inf.isblocked == 0 && inf.isblank == 0) score[j][i] = put_max(2, score[j][i]);
					if(inf.length == 2 && inf.isblocked == 1 && inf.isblank == 0) score[j][i] = put_max(1, score[j][i]);
				}

				for(int k = 0; k < 12; k++){
					inf = getinfo(dirlist[k][0], j, i, opp, my, board);
					inf2 = getinfo(dirlist[k][1], j, i, opp, my, board);

					if(inf.length == 4 && inf2.length == 4 && inf.isblank < 2 && inf2.isblank < 2) score[j][i] = put_max(99, score[j][i]);
					if(inf.length == 4 && inf2.length == 3 && inf2.isblocked == 0 && inf.isblank < 2 && inf2.isblank < 2) score[j][i] = put_max(98, score[j][i]);
					if(inf.length == 3 && inf.isblocked == 0 && inf2.length == 3 && inf2.isblocked == 0 && inf.isblank < 2 && inf2.isblank < 2) score[j][i] = put_max(30, score[j][i]);
					if(inf.length == 3 && inf.isblocked == 0 && inf2.length == 2 && inf2.isblocked == 0 && inf.isblank < 2) score[j][i] = put_max(21, score[j][i]);
					if(inf.length == 3 && inf.isblocked == 1 && inf2.length == 3 && inf2.isblocked == 0 && inf.isblank < 2 && inf2.isblank < 2) score[j][i] = put_max(8, score[j][i]);
					if(inf.length == 2 && inf.isblocked == 0 && inf2.length == 2 && inf2.isblocked == 0) score[j][i] = put_max(5, score[j][i]);
				}

			}
		}

		for(int i = 0; i < 19; i++){
			for(int j = 0; j < 19; j++){
				max2 = put_max(score[j][i], max2);
				if(max2 == score[j][i]){
					posx = j;
					posy = i;
				}
			}
		}

		cout << posx << ' ' << posy << endl;
	}
	return 0;
}